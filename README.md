# Setup the teacher portal repository

- `git clone https://gitlab.com/aartzy/teacher-portal-ui.git`
- `cd teacher-portal-ui/packages/teacher-portal-ui`
- `npm i`
- `npm run start`
