import { Component, OnInit } from '@angular/core';
import { Course } from '../common/interfaces/course.interface';
import { CourseService } from '../course/course.service';
import { FileService } from './file.service';

@Component({
  selector: 'app-file',
  templateUrl: './file.page.html',
  styleUrls: ['./file.page.scss'],
})
export class FilePage implements OnInit {
  courseList: Array<Course>;
  note: Course;
  selectedCourse: any;
  constructor(
    private courseService: CourseService,
    private fileService: FileService,
  ) {
    this.courseList = [];
    this.selectedCourse = { program: '', course_name: '' } as any;
    this.note = {} as Course;
  }

  ngOnInit() {
    if (this.courseService.selectedCourse.value.program !== '') {
      this.selectedCourse.program = this.courseService.selectedCourse.value.program;
      this.selectedCourse.course_name = this.courseService.selectedCourse.value.course_name;
      this.getNote();
    }
    this.getCourseList();
  }
  compareObjects(o1: any, o2: any): boolean {
    return o1.program == o2.program && o1.course_name == o2.course_name;
  }

  getCourseList() {
    this.courseService.getCourseList().subscribe({
      next: res => {
        this.courseList = res;
        if (this.courseService.selectedCourse.value.program === '') {
          this.selectedCourse =
            this.courseList && this.courseList.length
              ? this.courseList[0]
              : { program: '', course_name: '' };
        }
        this.getNote();
      },
    });
  }

  getNote() {
    this.fileService
      .getNote(this.selectedCourse.program, this.selectedCourse.course_name)
      .subscribe({
        next: res => {
          this.note = res;
        },
      });
  }
}
