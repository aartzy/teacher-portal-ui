import { Topic } from './topic.interface';
import { Note } from './note.interface';

export interface Course {
  program: string;
  course_name: string;
  completed?: number;
  remaining?: number;
  roomNo?: string;
  topics?: Topic[];
  currentTopic?: string;
  upcomingTopic?: string;
  notes?: Array<Note>;
}
