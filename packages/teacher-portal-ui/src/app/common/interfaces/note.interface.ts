import { Attachment } from './notefile.interface';

export interface Note {
  date: string;
  title: string;
  description?: string;
  filename?: Array<Attachment>;
}
export class APIResponse {
  docs: any[];
  length: number;
  offset: number;
}
