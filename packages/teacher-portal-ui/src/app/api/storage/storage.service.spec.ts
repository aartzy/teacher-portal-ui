import { TestBed } from '@angular/core/testing';

import { StorageService, STORAGE_TOKEN } from './storage.service';

describe('StorageService', () => {
  let service: StorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: STORAGE_TOKEN, useValue: {} }],
    });
    service = TestBed.inject(StorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
